#  仓库责任田细分

## 背景

本目录下存放的是 openGauss 社区中，需要多个 SIG 组去共同维护仓库信息

本目录下子目录 opengauss/openGauss-server，代表一个仓库。仓库文件夹下，会存在 <SIG名称>.yaml 文件。
每个 yaml 文件存放单个 SIG 维护的仓库文件清单。

###  sig-info.yaml 文件格式

sig-info.yaml 文件为yaml格式承载，包含如下基本元素：
| 字段 | 类型 | 说明 |
|--|--|--|
| file | 列表 | 仓库下文件夹或文件地址 |
| owner | 列表 | 维护 file 清单的 Owners |

其中 owner 列表中每一条记录代表一位维护者的个人信息，每一条个人信息记录包含如下元素：

| 字段 | 类型 | 说明 |
|--|--|--|
| gitee_id | 字符串 | gitee ID, 必填 |
| name | 字符串 | 姓名(或者网名), 必填 |
| organization| 字符串 | 所在组织或单位, 选填 |
| email| 字符串 | 个人邮箱地址, 必填 |
| opengauss_id | 字符串 | openeuler账号名称, 选填 |
| gitcode_id | 字符串 | openeuler账号名称, 选填 |

### <SIG名称>.yaml 文件样例
```
files:
  - file:
      - openGauss-server/liteom
      - openGauss-server/liteom/upgrade_common.sh
      - openGauss-server/liteom/opengauss_lite.conf
      - openGauss-server/liteom/upgrade_config.sh
      - openGauss-server/liteom/install.sh
      - openGauss-server/liteom/uninstall.sh
      - openGauss-server/liteom/upgrade_GAUSSV5.sh
      - openGauss-server/liteom/upgrade_errorcode.sh
    owner:
      - gitee_id: dscao
        organization: huawei
        name: caodongsheng
        email: caodongsheng2@huawei.com
```