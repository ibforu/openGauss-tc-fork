## SIG组信息维护指导文档

> 本文档是一个指导文档，旨在帮助社区参与者理解如何查询/维护SIG组的相关数据
> **SIG组**这一概念可参考 []
> SIG组的相关数据存储在代码托管平台上，以代码仓库的形式归档维护


### SIG组信息的代码仓库结构


```yaml

opengauss/tc
|   architecture.png
|   CONTRIBUTING.md
|   gauss_relationship.md   #fdasjkh;fa
|   gauss_relationship.yaml
|   LICENSE
|   maillist_mapping.yaml
|   OWNERS
|   README.en.md
|   README.md
|   repos.yaml
|   sig-member-repo-manage-guide.md
|   sigs.yaml
|   
+---.gitee
|       ISSUE_TEMPLATE.en.md
|       PULL_REQUEST_TEMPLATE.en.md
|       
+---repos
|   |   README.md
|   |   
|   \---opengauss
|       \---openGauss-server
|               OM.yaml
|               Tools.yaml
|               
\---sigs                      # SIG组数据的归档目录
    |   README.en.md          # SIG组的英文介绍文档
    |   README.md             # SIG组的中文介绍文档
    |   
    +---AI                    # 每一个文件夹对应一个SIG组
    |       OWNERS            # AI SIG 的 maintainers 和 committers
    |                         # 当只存在OWNERS文件时，以OWNERS为准
    |       README.md         # 对应的 AI SIG 的相关介绍文档
    |       
    +---...                   # 省略中间的SIG组              
    |      
    +---Connectors            # 对应 Connectors SIG 组
    |   |   OWNERS            # Connectors SIG 的 maintainers 和 committers
    |   |   README.md
    |   |   sig-info.yaml     # Connectors SIG 的 maintainers 和 committers、及其管理的仓库信息
    |   |                     # 当只存在sig-info.yaml文件时，以sig-info.yaml为准
    |   |                     # 当OWNERS和sig-info.yaml文件都存在时，以sig-info.yaml为准
    |   |   
    |   \---opengauss         # 此文件夹对应 Connectors SIG 管理的组织
    |       \---o             # 仓库按首字母归类管理
    |               openGauss-connector-jdbc.yaml      # 
    |               
    +---CTMM
    |       .keep
    |       OWNERS
    |       README.md
    |       
    +---DCF
    |       OWNERS
    |       README.md
    |       
    +---Docs
```


# 如何参与维护 SIG 的 YAML 文件

在 openGauss 开源社区，我们使用 YAML 文件管理各个专项兴趣小组（SIG）成员和代码仓的映射关系。为了确保信息的准确性与一致性，维护这些 YAML 文件是每个 SIG 成员的共同责任。本文档将帮助您了解如何参与到 YAML 文件的维护中。

## 1. 文件结构与内容概述

每个 SIG 都有一个对应的 YAML 文件，文件中包含以下几个主要部分：

- **name**: SIG 的名称。
- **description**: SIG 的描述，简要说明该 SIG 负责的领域或主题。
- **mailing_list**: SIG 的邮件列表。
- **meeting_url**: SIG 定期会议的链接（如果有）。
- **mature_level**: SIG 的成熟度，分为 `startup`、`mature` 等。
- **maintainers**: SIG 的维护人员列表，包含 Gitee ID、姓名、所属组织、邮箱等信息。
- **repositories**: SIG 管理的代码仓，包含代码仓名称和对应的提交人员（Committers）。

例如：
```yaml
name: Connectors
description: The Connectors team is Responsible for the planning, development and maintenance of database drivers
mailing_list: connectors@opengauss.org
meeting_url: NA
mature_level: startup
maintainers:
  - gitee_id: pikeTWG
    name: tianwengang
    organization: huawei
    email: tianwengang@huawei.com
repositories:
  - repo:
      - openGauss-server
      - openGauss-third_party
    committers:
      - gitee_id: pikeTWG
        name: tianwengang
        email: tianwengang@huawei.com
```

## 2. 如何更新维护人员

### 2.1 添加/更新维护人员

- **步骤**：
  1. 找到对应 SIG 的 YAML 文件。
  2. 在 `maintainers` 部分添加或更新维护人员的相关信息。
  3. 确保填写完整的信息，包括 Gitee ID、姓名、所属组织、邮箱等。

- **所需权限**：更新维护人员需要具有对应 SIG 的权限。通常，SIG 成员或 Maintainer 才能执行此操作。

- **审批流程**：添加或更新维护人员时，应该经过相应的 SIG 成员或 TC 的审批流程，确保新成员具备必要的权限。

### 2.2 删除维护人员

- **步骤**：
  1. 找到对应 SIG 的 YAML 文件。
  2. 从 `maintainers` 部分删除对应维护人员的记录。
  3. 提交变更。

- **所需权限**：只有 SIG 的 Maintainer 才能删除成员信息。

## 3. 如何更新代码仓

### 3.1 添加/更新代码仓

- **步骤**：
  1. 找到对应 SIG 的 YAML 文件。
  2. 在 `repositories` 部分添加或更新代码仓的信息。
  3. 确保填写所有仓库的名称及对应的提交人员（Committers）。

- **所需权限**：更新代码仓信息需要 SIG Maintainer 或 Committer 权限。

- **审批流程**：添加或更新代码仓时，必须经过相关 SIG 成员的审批。

### 3.2 删除代码仓

- **步骤**：
  1. 找到对应 SIG 的 YAML 文件。
  2. 从 `repositories` 部分删除对应的代码仓。
  3. 提交变更。

- **所需权限**：只有 SIG 的 Maintainer 才能删除代码仓信息。

## 4. 如何管理维护人员与代码仓的关系

每个维护人员（Maintainer 或 Committer）可能会与一个或多个代码仓建立绑定关系。以下是如何管理这些关系：

### 4.1 维护人员与代码仓的绑定

- **绑定关系示例**：
  - 一个 Committer 对一个仓库
  - 一个 Committer 对多个仓库
  - 多个 Committer 对一个仓库

- **步骤**：
  1. 找到对应 SIG 的 YAML 文件。
  2. 在 `repositories` 部分，为每个仓库列出对应的 Committers，并为每个 Committer 明确其负责的仓库。
  3. 更新绑定关系时，确保添加正确的 Gitee ID 和邮箱。

- **所需权限**：维护人员与仓库的关系更新需要 SIG Maintainer 或 Committer 权限。

### 4.2 特例：多个 SIG 共同管理一个仓库

例如，`openGauss-server` 仓库可能由多个 SIG 共同管理。此时，需要将不同 SIG 的成员与具体目录或文件关联起来。

- **步骤**：
  1. 确定哪些 SIG 将共同管理某个仓库或某个目录/文件。
  2. 在 YAML 文件中明确指定各 SIG 管理的资源及负责人。

- **所需权限**：需要各 SIG 的 Maintainer 一致同意。

## 5. 提交与审批流程

- **提交**：所有 YAML 文件的更改必须通过 Git 提交，并且必须确保提交信息清晰且准确，描述所做的更改内容。
- **审批**：在更改文件之前，需要与相关 SIG 的 Maintainer 或 TC 进行沟通，确保变更内容符合社区的整体要求。
- **CI 校验**：在提交更改后，确保通过 CI 流程验证 YAML 文件的格式正确性。

## 6. 总结

维护 SIG 的 YAML 文件是 openGauss 社区的重要工作之一，确保文件内容准确、及时更新有助于社区的顺利运行。如果您有任何疑问或需要帮助，请随时联系相关 SIG 成员或社区的管理人员。

--- 

这个文档应该能帮助新成员理解如何有效地管理和更新 YAML 文件。如果您有更具体的需求或问题，可以进一步补充细节。