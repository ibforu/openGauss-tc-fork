# Embedded

Embedded是

# 组织会议

- 公开的会议时间：北京时间 每双周X 1X:00-1X:00

# 成员

### Maintainer列表

- 刘保玉[@borisfly](https://gitee.com/borisfly), *liubaoyu@ncti-gba.cn*
- 吴峰[@aiminralph](https://gitee.com/aiminralph), *wufeng@ncti-gba.cn*
- 阙鸣健[@Mijamind](https://gitee.com/Mijamind), *quemingjian2@huawei.com*

### Committer列表

- 窦欣[@ywzq1161327784](https://gitee.com/ywzq1161327784), *xdou1995@163.com*
- 申正[@shenzheng4](https://gitee.com/shenzheng4), *shenzheng4@huawei.com*
- 李振[@lizhen29](https://gitee.com/lizhen29), *lizhen@ncti-gba.cn*
- 孙天飞[@telebby](https://gitee.com/telebby), *suntianfei@ncti-gba.cn*
- 李玉祥[@zxccxz](https://gitee.com/zxccxz), *liyuxiang@ncti-gba.cn*
- 蔡文艺[@vanbars](https://gitee.com/vanbars), *caiwenyi@ncti-gba.cn*
- 杨子浩[@Louisyzh](https://gitee.com/Louisyzh), *yangzihao@ncti-gba.cn*
- 曾色亮[@zengseliang](https://gitee.com/zengseliang), *zengseliang@ncti-gba.cn*

# 联系方式

- [邮件列表](待补充)

# 仓库清单

仓库地址：

- 待补充


