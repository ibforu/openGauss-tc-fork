# 专项兴趣小组（Special Interest Groups，简称SIG）

SIG工作目标为针对特定的一个或多个主题建立社区项目，并推动各项目输出交付成果。各SIG的成立都需经TC决策，并建立治理规则，明确规定该SIG的职责，内容应包括但不限于其职责范围、沟通方式、维护人员任免（Maintainer、Committer等）、业务范围（代码库、目录等）等信息。同时，各SIG内部沟通信息必须在openGauss社区范围内公开，以确保其他SIG的社区成员可以找到讨论、会议和决策等相关记录。

## SIG组运作规则

SIG组应该在gitee建立自己的主页，包含必需的内容，SIG组可以根据实际情况定制适合自己的规则，模板如下：


### xx SIG组主要负责事项

xx SIG的主要责任是负责openGauss社区xx模块的开发和维护...


### xx SIG组会议

公开的视频会议时间：北京时间双周X下午，14:00~16:00，请订阅https://opengauss.org/zh/community/onlineCommunication/ ，以获取相关会议通知

### 会议纪要归档

xx SIG历次会议纪要均做归档，详情请查询历史会议纪要归档etherpad

### 成员

**Maintainer：**
| 姓名| GiteeID           | 邮件地址  |
| :-------------: |:--------:| :-----|
|xx | [@Cyj10727](https://gitee.com/Cyj10727)| jieky.cai@huawei.com|
|xx |[@wang-jingle](https://gitee.com/wang-jingle)|wangjiang16@huawei.com|
|xx  | [@flowill](https://gitee.com/flowill)|  f.fengwei@huawei.com|

**Commiter:**
| 姓名| GiteeID           | 邮件地址  |
| :-------------: |:--------:| :-----|
|xx| [@xiong_xjun](https://gitee.com/xiong_xjun) | xiong_xiaojun@yeah.net|
|xx| [@willloong](https://gitee.com/willloong) | 1259931895@qq.com|
|xx|[@llzx373](https://gitee.com/llzx373) | llzx373@hotmail.com|
|xx|[@zhang_xubo](https://gitee.com/zhang_xubo) | 2578876417@qq.com|

### Maintainer职责：
- 负责看护SIG项目架构设计、保证SIG项目架构代码质量，拥有SIG项目的代码检视和合入代码权限，定期组织、召集社区SIG项目组例会，代表SIG参加技术委员会组织的活动和会议。

### xx SIG Maintainer竞选流程及原则
由maintainer和SIG成员提名（个人先向以上成员提出申请），xx成员提名（个人先向以上成员提出申请），xx sig评审
- 作为Committer至少3个月，自荐或者由现有Maintainer、TC提名，并且没有其他TC和Maintainer反对。
- 个人或者所属团队，连续6个月以上对社区有持续贡献，且至少有一个特性为版本关键特性（存在于release notes）
- 持续参加xx sig例会，近6个月与会率70%以上
- 对版本xx sig管理有突出贡献（自我举证）

### xx SIG Maintianer变更流程及原则
主动提出或者xx sig提出，在sig评审
- 主动退出或者连续3次以上无故不参与xx SIG例会
- 个人或者所属团队，连续6个月无社区贡献

### Committer职责：
- 负责代码检视、保证SIG项目代码质量，拥有SIG项目的代码检视和合入代码权限，定期参加Maintainer组织的SIG项目组例会。

### xx SIG Committer竞选流程及原则
由maintainer和SIG成员提名（个人先向以上成员提出申请），xx sig评审
- 作为openGauss社区Contributor至少3个月，由Maintainer或Committer提名，并且没有其他Maintainer和Committer反对。
- 需要独立完成至少一个功能或修复一个重大Bug
- 持续参加xx sig例会，近6个月与会率70%以上
- 对版本xx sig有突出贡献（自我举证）

### xx SIG Committer变更流程及原则
主动提出或者xx sig提出，在sig评审
- 主动退出或者连续3次以上无故不参与xx SIG例会
- 连续6个月无社区贡献

### 联系方式
| SIG ID| Gitee 访问链接           | 邮件地址  |
| :-------------: |:--------:| :-----|
|xx SIG|[xx](https://gitee.com/opengauss/release-management/)|xx|



### 项目清单

Repository地址：https://gitee.com/opengauss/release-management
...



## 现有SIG组
* 各SIG成员应包含一至两名Maintainer，多名Committer及Contributor。openGauss社区SIG包括：

    | SIG名称 | 职责范围 |
    | :------- | :--------------- |
    | SQLEngine | 负责openGauss社区SQL引擎的开发和维护。 |
    | StorageEngine | 负责openGauss社区存储引擎的开发和维护。 |
    | Connectors | 负责openGauss社区Connectors的开发和维护。 |
    | Tools | 负责openGauss社区工具的开发和维护。 |
    | Docs | 负责openGauss社区文档的开发和维护。 |
    | Infra | 负责openGauss社区基础设施的开发和维护。 |
    | Security | 负责openGauss社区安全的开发和维护。 |
    | OM | 负责openGauss社区部署和维护。 |
    | AI | 负责openGauss社区AI的开发和维护。 |
    | IoT | 负责openGauss社区IoT能力的开发和维护。 |
    | In-place Update | 负责openGauss社区in-place update的开发和维护。 |
    | GIS | 负责openGauss社区地理信息系统的开发和维护。 |
    | CloudNative | 负责openGauss社区云原生方向的开发和维护。 |
    | SecurityTechnology | 负责openGauss社区数据库安全技术的开发和维护。 |
    | Certification | 负责openGauss认证流程、测试套件的定义和开发。 |
    | Plugin | 负责openGauss插件机制的规划、管理、开发等。 |
    | Blockchain | 探讨区块链的业务场景，研究区块链的核心技术问题。 |
    | DCF | 负责openGauss社区分布式一致性框架DCF的开发和维护。 |
    | QA | 负责openGauss社区版本质量相关的开发和维护。 |
    | Graph | 负责openGauss社区统一存储和查询的知识图谱数据管理功能。 |
    | ReleaseManagement | 社区协同各SIG maintainer，规划openGauss社区版本的发布工作，为最终的竞争力目标达成负责. |
    | CM | 负责openGauss社区公共组件，包括集群管理、共享存储、分布式一致性框架、分布式配置中心等的开发和维护。 |
    | OPS | 聚焦 openGauss 维护能力建设，提升产品运维能力，总结和传递运维经验。 |
    | KnowledgeGraph | 围绕知识图谱全生命周期：构建、存储管理与应用环节开展研究。 |

## 背景

本目录下存放的是 openGauss 社区中，所有代码仓与特别兴趣小组 （Special Interest Group，以下简称 SIG）的运作信息。

本目录下每一个子目录，代表一个 SIG。每个 SIG 的目录中，会存在 OWNERS 或 sig-info.yaml 文件；sig-info.yaml 版本新于 OWNERS；优先使用新版本的信息。
每个 SIG 目录中，存在 opengauss 子目录；所有被该 SIG 管理的代码仓，都以相应的独立 YAML 文件描述，存放于这个子目录中（按首字母细分）。


## 数据存放和管理方式

1. 每个 SIG 在 opengauss/tc 仓的 sigs 目录中各有一个独立的目录，目录下必须至少存在 OWNERS 文件与 sig-info.yaml 文件其中之一。
2. 原则上由 技术委员会 修改和维护。各个 SIG 所对应的 sig-info.yaml 的修改，由各 SIG 的 maintainer 提交PR，经过技术委员会审视后合入。
3. 信息的权威性，sig-info.yaml > OWNERS > 其他。如果出现信息不一致，以排序最先的 YAML 文件中信息为准。
4. 各 SIG 独立目录下的 README.md 为 SIG 的信息展示区。其中 SIG 基本信息需按模板留空，由工具自动填充。

## 格式规范

### OWNERS 文件格式规范
OWNERS 仅包含 maintainer 和 committers 列表。

### OWNERS 文件样例
```
maintainers:
- %gitee_id%
- %gitee_id2%
committers:
- %gitee_id3%
- %gitee_id4%
```

###  sig-info.yaml 文件格式

sig-info.yaml 文件为yaml格式承载，包含如下基本元素：
| 字段 | 类型 | 说明 |
|--|--|--|
| name | 字符串 | SIG组名称 |
| description | 字符串 | SIG组描述信息 |
| mailing_list | 字符串 | SIG组讨论邮件列表地址 |
| meeting_url | 字符串 | SIG例会纪要URL |
| mentors | 列表 | SIG组当前导师名单 |
| maintainers | 列表 | SIG组所有maintainer名单 |
| security_contacts | 列表 | SIG组安全接口人名单 |
| committers | 列表 | SIG组所有committer名单 |
| contributors | 列表 | SIG组管理仓库的责任人名单 |
| repositories| 列表 | SIG组所管辖的代码托管平台仓库信息 |

其中 mentors 列表中每一条记录代表一位 mentor 的个人信息，maintainers 列表中每一条记录代表一位 maintainer 的个人信息， security_contacts 列表中每一条记录代表一位 security_contact 的个人信息。每一条个人信息记录包含如下元素：

| 字段 | 类型 | 说明 |
|--|--|--|
| gitee_id | 字符串 | gitee ID, 必填 |
| name | 字符串 | 姓名(或者网名), 必填 |
| organization| 字符串 | 所在组织或单位, 选填 |
| email| 字符串 | 个人邮箱地址, 必填 |
| opengauss_id | 字符串 | openeuler账号名称, 选填 |
| gitcode_id | 字符串 | openeuler账号名称, 选填 |

其中 repositories 列表中每一条记录为SIG所管理的一组仓库信息：

| 字段 | 类型 |  说明 |
|--|--|--|
| repo | 字符串 | 一组SIG仓库 |


repositories的每个repo均由一组具体的仓库名以及committers和contributors组成：
| 字段 | 类型 |  说明 |
|--|--|--|
|  | 字符串 | 仓库全名 |
| committers | 列表 | 参与代码审核的人员 |
| contributors | 列表 | 参与代码仓贡献的其他开发人员 |

其中committers拥有该repo下所有仓库的代码审核权限，contributors是该repo下所有仓库的责任人。committers 列表中每一条记录代表一位 committer 的个人信息， contributors 列表中每一条记录为 仅参与该代码仓的贡献人员个人信息：

### sig-info.yaml 样例：
```
name: Infra
description: This is a sample sig. Please copy it over and modify it accordingly.
mailing_list: infra@opengauss.org
meeting_url: NA
mature_level: startup
mentors:
- gitee_id: AAAAA
  name: BBBBB
  email: aaaaaaa@opengauss.org
maintainers:
- gitee_id: Joe
  name: JoeDou
  organization: RealT
  email: yyyyyyy@qq.com
- gitee_id: Jane
  name: JaneDou
  email: xxxxxxx@gmail.com
- openeuler_id: Jane
repositories:
- repo:
  - opengauss/openGauss-server
  - openeuler/openGauss-connector-odbc
- repo:
  - openeuler/infra
  - openeuler/website
  committers:
  - gitee_id: Bob
    name: BobMa
    email: zzzzzzz@yahoo.com
  contributors:
  - gitee_id: infra_superman
    name: Clark_Ken
    organization: Justice_L
    email: zzzzzzz@opengauss.org
```

### 代码仓描述文件格式

配置文件整体以yaml格式承载，包含如下基本元素：

| 名称 | 类型 | 说明 |
| :-- | :-- | :-- |
| name|字符串|仓库名称|
| rename_from|字符串|仓库原名称。这个子元素为可选，只有该代码仓是从另一个代码仓改名而来时才需要|
| description| 字符串 | 仓库包含组件的描述 |
| type|枚举类型，可选 public 或者 private | 仓库类型。private代码仓不提供开放访问|
|upstream|字符串|本代码仓对应的上游社区信息。当 community 为 src-openeuler时，这个子元素必须提供；当 community 为 openeuler 且项目本身就是社区原创项目时，可以不设置|
| branches|清单|本代码仓下所有分支信息|

branches 清单中每个元素代表一个受管理的分支，以关系数组的方式呈现，需要包含以下子元素:
| 名称 | 类型 | 说明 |
| :-- | :-- | :-- |
| name| 字符串 | 分支名称 |
| type | 枚举类型，可选protected/readonly |  分支类型，对照码云分支属性设置，protected 表示该分支可以被发布版本集成，readonly 表示该分支停止维护 |
| create_from | 字符串 | 分支创建起点，当 branches.name 为 master 时，字符串为空；新创其他分支时设置已存在的分支名或tag名，缺省为master |

### 代码仓描述文件样例
```
- name: DB
  description: 'This is a repo for ……'
  branches:
  - name: master
    type: protected
  - 1.0.0
    type: readonly
    create_from: master
  - 2.0.0
    type: protected
    create_from: master
  type: public
```